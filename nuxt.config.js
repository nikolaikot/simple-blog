// eslint-disable-next-line nuxt/no-cjs-in-config
const bodyParser = require('body-parser')
export default {
  mode: 'universal',
  /*
   ** Headers of the page
   */
  head: {
    title: 'WD Blog',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: 'My coll web development blog'
      }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      {
        rel: 'stylesheet',
        href:
          'href="https://fonts.googleapis.com/css?family=Open+Sans&display=swap"'
      }
    ]
  },
  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#fa923f', height: '4px' },
  /*
   ** Global CSS
   */
  css: ['~assets/styles/main.css'],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: ['~plugins/core-components.js', '~plugins/date-filter.js'],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module'
  ],
  /*
   ** Nuxt.js modules
   */
  modules: ['@nuxtjs/axios'],

  axios: {
    baseURL: process.env.BASE_URL || 'https://nuxt-blog-45f3f.firebaseio.com',
    credentials: false
  },
  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {}
  },
  env: {
    baseUrl: process.env.BASE_URL || 'https://nuxt-blog-45f3f.firebaseio.com',
    fbAPIKey: 'AIzaSyBt1eshSX0oU1_B99KQP6J2uwM234KGRUA'
  },
  transition: {
    name: 'fade',
    mode: 'out-in'
  },
  serverMiddleware: [bodyParser.json(), '~/api']
}
